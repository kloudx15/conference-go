# Generated by Django 4.0.3 on 2022-06-06 02:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0002_accountvo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountvo',
            name='email',
            field=models.EmailField(max_length=200),
        ),
        migrations.AlterField(
            model_name='accountvo',
            name='is_active',
            field=models.BooleanField(),
        ),
        migrations.AlterField(
            model_name='accountvo',
            name='updated',
            field=models.CharField(max_length=25),
        ),
    ]
