import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_message_approval(ch, method, properties, body):
    content = json.loads(body)
    print(content)
    return send_mail(
        "Your presentation has been accepted,"
        f"{content['presenter_name']}, we're happy to tell you that your presentation {content['title']} has been accepted",
        "admin@conference.go",
        [content["presenter_email"]],
        fail_silently=False,
    )


def process_message_rejection(ch, method, properties, body):
    content = json.loads(body)
    print(content)
    return send_mail(
        "your presentation has been rejected",
        f"{content['presenter_name']}, thank you for your consideration in choosing us as a host. Unfortunately, your presentation {content['title']} was rejected.",
        "admin@conference.go",
        [content["presenter_email"]],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_message_approval,
            auto_ack=True,
        )
        channel.start_consuming()
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_message_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
